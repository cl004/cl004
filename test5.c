# include<stdio.h>
int main()
{
	int arr[100], min, max;
	int n, i, num1 = 0, num2 = 0, position1 = 0, position2 = 0;
	printf("Enter the num of elements :\n ");
	scanf("%d", &n);
	printf("Enter the elements :\n");
	for (i = 0; i<n; i++)
	{
		scanf("%d", &arr[i]);
		if (i == 0)
		{
			min = max = arr[i];
		}
		if (arr[i]<min)
			min = arr[i];
		else if (arr[i]>max)
			max = arr[i];
	}
	printf("\nBiggest element is %d and Smallest element is %d\n ", max, min);

	for (i = 0; i<n; i++)
	{
		if (min == arr[i])
		{
			position1 = i;
			break;
		}
	}
	for (i = 0; i<n; i++)
	{
		if (max == arr[i])
		{
			position2 = i;
			break;
		}
	}
	num1 = arr[position1];
	num2 = arr[position2];
	arr[position1] = num2;
	arr[position2] = num1;
	printf("\nArray after interchange of smallest and largest : \n");
	for (i = 0; i<n; i++)
	{	
		printf(" arr[%d] = %d\n",i,arr[i]);
		}
	return 0;
}

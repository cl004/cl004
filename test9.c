#include<stdio.h>
void swap_reference(int*,int*);
void swap_value(int,int);
int main()
{
int p,q,c,d;
printf("Enter the 4 integers\n");
scanf("%d%d%d%d",&p,&q,&c,&d);
printf("The value before swaping using reference are %d,%d\n",c,d);
swap_reference(&c,&d);
printf("The value before swaping using value are %d,%d\n",p,q);
swap_value(p,q);
return 0;
}

void swap_reference(int *c,int *d)
{
	int temp1;
temp1 = *c;
*c = *d;
*d = temp1;
printf("The value after swaping using reference are %d,%d\n",*c,*d);
}
void swap_value(int p,int q)
{
	int temp2;
temp2 = p;
p = q;
q = temp2;
printf("The value after swaping using value are %d,%d\n",p,q);
}


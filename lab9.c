#include<stdio.h>
int main()
{
int a,b,*p,*q,add,sub,multi,div,rem;
printf("Enter the two integers\n");
scanf("%d%d",&a,&b);
p = &a;
q = &b;
add = *p+*q;
sub =  *p-*q;
multi =  *p*(*q);
div =  *p/(*q) ;
rem =  *p%(*q) ;
printf("The sum of the two integers %d and %d are %d\n",*p,*q,add);
printf("The difference of the two integers %d and %d are %d\n",*p,*q,sub);
printf("The product of the two integers %d and %d are %d\n",*p,*q,multi);
printf("The division of the two integers %d and %d are %d\n",*p,*q,div);
printf("The reminder of the two integers %d and %d are %d\n",*p,*q,rem);
return 0;
}
